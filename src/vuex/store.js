import Vue from 'vue'
import Vuex from 'vuex'
import { categories } from '../resources/categories.json'
import { products } from '../resources/products.json'

Vue.use(Vuex)

const localKey = 'cart';

const saveToLocalStorage = (shoppingCart) => localStorage.setItem(localKey, JSON.stringify(state.shoppingCart))

const state = {
  categories,
  products: products.map(product => {
    product.price = parseInt(product.price.replace(/[^0-9\.]/g, ''), 10)
    return product;
  }),
  shoppingCart: JSON.parse(localStorage.getItem(localKey)) || []
}

const getters = {
  getProductsBySublevel: (state) => (sublevelId) => {
    return state.products.filter(product => product.sublevel_id == sublevelId)
  },
  filterProductsSublevel: (state) => (sublevelId, availability, priceRange, quantityRange, nameKeyword) => {
    return state.products.filter(product => {
      let condition = product.sublevel_id == sublevelId
      if (typeof(availability) === typeof(true)) {
        condition &= product.available == availability
      }
      if (priceRange) {
        condition &= product.price >= priceRange[0] && product.price <= priceRange[1]
      }
      if (quantityRange) {
        condition &= product.quantity >= quantityRange[0] && product.quantity <= quantityRange[1]
      }
      if (nameKeyword) {
        condition &= product.name.includes(nameKeyword)
      }
      return condition
    })
  },
  getShoppingCartProducts: (state) => state.shoppingCart,
  shoppingCartTotal: (state) => state.shoppingCart.reduce((total, item) => total + (item.quantity * item.product.price), 0),
  findSublevel: (state) => (categoryId, sublevelId) => {
    const category = state.categories.find(c => c.id == categoryId)
    let stack = category.sublevels.slice(0)
    while (stack.length > 0) {
      const sublevel = stack.pop()
      if (sublevel.id == sublevelId) {
        return sublevel
      }
      if (sublevel.sublevels) {
        stack = stack.concat(sublevel.sublevels)
      }
    }
    return null
  },
  getShoppingCartProduct: (state) => (product) => {
    return state.shoppingCart.find(i => {
      return i.product.id == product.id
    })
  }
}

const mutations = {
  addProductToCart: (state, payload) => {
    const item = state.shoppingCart.find(d => d.product.id == payload.id)
    if (!state.shoppingCart.find(d => d.product.id == payload.id)) {
      state.shoppingCart.push({product: payload, quantity: 1})
    } else if (item.product.quantity > item.quantity) {
      item.quantity++
    }
  },
  deleteProductCart: (state, product) => {
    state.shoppingCart.splice(state.shoppingCart.indexOf(product), 1)
  },
  editProductCartQuantity: (state, payload) => {
    const res = state.shoppingCart.find(p => p.product.id == payload.item.product.id)
    if (res) {
      res.quantity = payload.quantity
    }
  },
  buy: (state) => {
    state.shoppingCart.forEach(item => {
      const prod = state.products.find(p => p.id == item.product.id)
      prod.quantity -= item.quantity
    })
    state.shoppingCart = []
    localStorage.removeItem(localKey)
  }
}

const store = new Vuex.Store({
  state,
  getters,
  mutations
})

store.watch((state) => state.shoppingCart, (val) => saveToLocalStorage(val.map(v => {
  return {product: v.product, quantity: v.quantity}
})), {deep: true})

export default store