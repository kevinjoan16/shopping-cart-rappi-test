import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import 'vuetify/dist/vuetify.css'
import HomeComponent from './components/home'
import SublevelComponent from './components/sublevel-products'
import store from './vuex/store'
import VueCurrencyFilter from 'vue-currency-filter'

Vue.use(VueRouter)
Vue.use(Vuetify)
Vue.use(VueCurrencyFilter, {
    symbol: '$',
    thousandsSeparator: ',',
    fractionCount: 0,
    symbolPosition: 'front',
    symbolSpacing: true
  })

const routes = [
    {name: 'home', path: '/', component: HomeComponent},
    {name: 'sublevel-products', path: '/categories/:categoryId/sublevels/:sublevelId/products', component: SublevelComponent}
]

const router = new VueRouter({
    routes
})

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
