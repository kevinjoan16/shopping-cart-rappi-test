# MiTienda

> A Vue.js and Vuetify project made with love

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
# This will generate a js file in dist, just open the index.html in a browser
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
